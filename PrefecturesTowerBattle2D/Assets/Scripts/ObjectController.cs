﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectController : MonoBehaviour
{
    public static List<Moving> isMoves = new List<Moving>();//移動してる動物がいないかチェックするリスト

    private Rigidbody2D rigid;
    Moving moving = new Moving();//移動チェック変数

    // Use this for initialization
    void Start()
    {
        /// <summary>
        /// リストに追加&Rigidbody2D取得
        /// </summary>
        rigid = GetComponent<Rigidbody2D>();
        isMoves.Add(moving);
    }

    // Update is called once per frame
    void Update()
    {
        /*これ使うのかな？
         * public float speed = 2;
        float x = Input.GetAxisRaw("Horizontal");
        */
       
        if (false = isHit)
        {
            return;
        }

       
            //左が押されたときに左に移動
            if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Translate(-2, 0, 0, Space.World);
            //transform.position = new Vector3(-2f, 0f, 0.0f);
            //transform.position = new Vector3(-2, 0, 0);

        }

        //右が押されたときに右に移動
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Translate(2, 0, 0, Space.World);
            //transform.position = new Vector3(2f, 0f, 0.0f);
            //transform.position = new Vector3(2, 0, 0);
        }

        // 上が押されたときに左に移動
        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.Rotate(new Vector3(0, 0, 1.5f));
        }

        // 下が押されたときに右に移動
        if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.Rotate(new Vector3(0, 0, -1.5f));
        }


    }
    /// <summary>
    /// 固定フレームレートで移動チェック
    /// </summary>
    private void FixedUpdate()
    {
        if (rigid.velocity.magnitude > 0.01f)//少しでも移動していれば動いてると判定
        {
            //Debug.Log("動いてる");
            moving.isMove = true;
        }
        else
        {
            //Debug.Log("動いていない");
            moving.isMove = false;
        }
    }

   private void OnCollisionStay2D(Collision2D collision)
    {
        Debug.Log("hit Object");
        collision == isHit;
    }
    



    /// 移動チェッククラス
    /// 理由：bool型をリストで渡すと値として認識されてしまった。
    /// 苦肉の策として、参照してもらうためにクラスを作った。
    public class Moving
    {
        public bool isMove;
    }

}
