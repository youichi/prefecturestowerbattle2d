﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    [SerializeField] private GameObject parentObject;   //親
    [SerializeField] private GameObject itemObject;     //落ちてくるアイテム
    [SerializeField] private List<GameObject> itemObjects;     //落ちてくるアイテム
    private Rigidbody2D rigid;


    public void onClickItemCreate()
    {
        // GameObject tmp = GameObject.Instantiate(itemObject, new Vector3(0f, 100f, 0f), Quaternion.identity);
        //tmp.transform.SetParent(parentObject.transform, true);
        //tmp.transform.localPosition = new Vector3(0f, 100f, 0f);

        int pickup = Random.Range(0, 10);

        //		GameObject tmp = GameObject.Instantiate(itemObjects[0], new Vector3(0f, 0f, 0f), Quaternion.identity);
        //		GameObject tmp = GameObject.Instantiate(itemObjects[9], new Vector3(0f, 0f, 0f), Quaternion.identity);
        GameObject tmp = GameObject.Instantiate(itemObjects[pickup], new Vector3(0f, 200f, 0f), Quaternion.identity);
        tmp.transform.SetParent(parentObject.transform, true);
        tmp.transform.localPosition = new Vector3(0f, 200f, 0f);

        // 移動させたいオブジェクトを取得
        //GameObject obj = GameObject.Find("PlayerObject");
        // オブジェクトを移動
        //obj.transform.Rotate(45f, 0f, 0f);

 
    }

    
   /* public void OnClick()
    {
        Rigidbody2D r = GetComponent(typeof(Rigidbody2D)) as Rigidbody2D;
        r.gravityScale = 10f;
    }
    */
}
